# Card War Game

[![pipeline status](https://gitlab.com/artemdwo/card-war/badges/master/pipeline.svg)](https://gitlab.com/artemdwo/card-war/-/commits/master)

## Description

The objective of the game is to win all of the cards.

The deck is divided evenly among the players, giving each a down stack. In unison, each player reveals the top card of their deck—this is a "battle"—and the player with the higher card takes both of the cards played and moves them to their stack. Aces are high, and suits are ignored.

If the two cards played are of equal value, then there is a "war". Both players place the next three cards face down and then another card face-up. The owner of the higher face-up card wins the war and adds all the cards on the table to the bottom of their deck. If the face-up cards are again equal then the battle repeats with another set of face-down/up cards. This repeats until one player's face-up card is higher than their opponent's.

[More details from Wikipedia](https://en.wikipedia.org/wiki/War_(card_game))

## Requirements

* Python 3+

## Run tests
`python -m unittest test_actors.py`

or 

`python3 -m unittest test_actors.py`

## Run the game
`python main.py`

or 

`python3 main.py`